import React, { useContext, useEffect, useState } from "react";

// import component here
import ButtonComp from "../../components/Button/Button";

// import styling here
import "./Home.css";

// import context here

const Home = () => {
  return (
    <>
      <h1>Home Page</h1>
      <ButtonComp
        className="btn btn-success mx-1"
        label="Back to landing"
        href="/"
      />
    </>
  );
};

export default Home;
