import React, { useContext, useEffect, useState } from 'react';
import { Form, Button, Container, Row, Col, Image, Table, Card, Accordion } from 'react-bootstrap';

// import component here
import BackgroundComp from '../../components/Background/Background';
// import styling here
import './UserProfile.css';

// import context here

const UserProfile = () => {
  return (
    <>
      <BackgroundComp background={'url(/img/Log_Reg2.jpg)'}>
        <div className="container">
          <div className="greetings">
            <h1>User Profile Page</h1>
          </div>
          <hr />
          <div className="row">
            <div className="col-md-12 col-lg-12 user-picture">
              <Image className="img" src="https://i.insider.com/5db1c746dee0192ce42bd6f7" width="300px" height="300px" border-radius="50%" />
            </div>
            <div className="user-data">
              <Accordion defaultActiveKey="0">
                <Card>
                  <Card.Header>
                    <Accordion.Toggle as={Button} variant="link" eventKey="0">
                      About Me
                    </Accordion.Toggle>
                  </Card.Header>
                  <Accordion.Collapse eventKey="0">
                    <Card.Body>Elon Reeve Musk FRS (/ˈiːlɒn/ EE-lon; born June 28, 1971) is a business magnate, industrial designer, and engineer.</Card.Body>
                  </Accordion.Collapse>
                </Card>
              </Accordion>
              <Table striped bordered hover size="sl">
                <thead>
                  <tr>
                    <th>First Name</th>
                    <th>Last Name</th>
                    <th>Username</th>
                    <th>Email</th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>Elon</td>
                    <td>Musk</td>
                    <td>musky</td>
                    <td>elon@binar.co.id</td>
                  </tr>
                </tbody>
              </Table>
              <Button variant="warning">Edit profile</Button>{' '}
            </div>
          </div>
        </div>
      </BackgroundComp>
    </>
  );
};

export default UserProfile;
