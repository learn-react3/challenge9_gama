import React, { useContext, useEffect, useState } from "react";

// import component here
import ModalComp from "../../components/Modal/Modal";
import ButtonComp from "../../components/Button/Button";

// import styling here
import "./Landing.css";

// import context here

const Landing = () => {
  return (
    <>
      <h1>#PushAjaDulu</h1>
      <ButtonComp className="btn btn-success mx-1" label="Test Button" />

      <ModalComp
        modalButtonOpen="primary"
        modalButtonClose="secondary"
        modalButtonSave="primary"
        modalHeading="Ini Modal Heading"
        modalButtonOpenLabel="Launch Demo Modal"
        modalButtonCloseLabel="Close"
        modalButtonSaveLabel="Save changes"
      />
      <ButtonComp
        className="btn btn-warning mx-1"
        label="Test Link Button"
        href="/home"
      />
    </>
  );
};

export default Landing;
