import React, { useContext, useEffect, useState } from 'react';

// import component here
import LoginComp from '../../components/Login/LoginComp';
import BackgroundComp from '../../components/Background/Background';

// import styling here
import './Login.css';
import { Container, Col, Row } from 'react-bootstrap';

// import context here
import { AuthContext } from '../../context/AppContext';

const Login = () => {
  // useContext
  const { signIn } = useContext(AuthContext);

  // useState
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');

  const [dataLog, setDataLog] = useState({
    email: '',
    password: '',
  });

  const onSubmit = async (e) => {
    e.preventDefault();
    setDataLog({
      ...dataLog,
      email: email,
      password: password,
    });

    console.log(dataLog);
    try {
      await signIn(dataLog);
    } catch (err) {
      console.error(err.message);
    }
  };

  return (
    <>
      <BackgroundComp background={'url(/img/Log_Reg2.jpg)'}>
        <Container className="mt-5">
          <Row>
            <Col md={7}>
              <p className="pLog">One Step Closer</p>
            </Col>
            <Col className="App" md={4}>
              <LoginComp setEmail={setEmail} setPassword={setPassword} handleSubmit={onSubmit} />
            </Col>
          </Row>
        </Container>
      </BackgroundComp>
    </>
  );
};

export default Login;
