import React, { useContext, useEffect, useState } from "react";

// import component here
import RegisterComp from "../../components/Register/RegisterComp";
import BackgroundComp from "../../components/Background/Background";

// import styling here
import "./Register.css";
import { Container, Col, Row } from "react-bootstrap";
import { Button } from "react-bootstrap";

// import context here

const Register = () => {
  // useContext

  // useState
  const [name, setName] = useState("");
  const [userName, setUserName] = useState("");
  const [email, setEmail] = useState("");
  const [password, setPassword] = useState("");

  const [dataReg, setDataReg] = useState({
    name: "",
    userName: "",
    email: "",
    password: "",
  });

  const onSubmit = (e) => {
    e.preventDefault();
    setDataReg({
      ...dataReg,
      name: name,
      userName: userName,
      email: email,
      password: password,
    });
  };

  const signOut = (e) => {
    e.preventDefault();
  };

  return (
    <>
      <BackgroundComp background={"url(/img/Log_Reg2.jpg)"}>
        <Container className="mt-5">
          <Row>
            <Col md={7}>
              <p className="pReg">Join to The Legendary Games</p>
            </Col>
            <Col className="App" md={4}>
              <RegisterComp
                setName={setName}
                setUserName={setUserName}
                setEmail={setEmail}
                setPassword={setPassword}
                handleSubmit={onSubmit}
              />
            </Col>
          </Row>
          <Button variant="warning" onClick={signOut}>
            sign out
          </Button>
        </Container>
      </BackgroundComp>
    </>
  );
};

export default Register;
