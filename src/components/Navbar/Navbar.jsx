import React from 'react';

// import components

// import styling
import { Navbar, Nav } from 'react-bootstrap';

const NavbarComp = (props) => {
  return (
    <>
      <Navbar collapseOnSelect expand="lg" bg={props.navBg} variant="dark">
        <Navbar.Brand href="#home">Team 2</Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="ml-auto">
            <Nav.Link href={`/${props.nav1}`} className="mx-4">
              {props.nav1Label}
            </Nav.Link>
            <Nav.Link href={`/${props.nav2}`} className="mx-4">
              {props.nav2Label}
            </Nav.Link>
            <Nav.Link href={`/${props.nav3}`} className="mx-4">
              {props.nav3Label}
            </Nav.Link>
            <Nav.Link href={`/${props.nav4}`} className="mx-4">
              {props.nav4Label}
            </Nav.Link>
            {props.withLogoutButton && (
              <Nav.Link className="mx-4" onClick={() => props.onLogOut()}>
                Logout
              </Nav.Link>
            )}
          </Nav>
        </Navbar.Collapse>
      </Navbar>
    </>
  );
};

export default NavbarComp;
