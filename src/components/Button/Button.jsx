import React from "react";

// import components

// import styling
import { Button } from "react-bootstrap";

const ButtonComp = (props) => {
  return (
    <>
      <Button
        variant={props.variant}
        className={props.className}
        onClick={props.handleClick}
        href={props.href}
      >
        {props.label}
      </Button>
    </>
  );
};

export default ButtonComp;
