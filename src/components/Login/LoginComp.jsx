import React from "react";
import { Link } from "react-router-dom";

// import component here
import ButtonComp from "../Button/Button";

// import styling here
import "./LoginComp.css";
import { FormControl } from "react-bootstrap";

// import context here

const LoginComp = (props) => {
    return (
      <>
        <div className="form-log">
          <h2>Login</h2>
          <form>
            
            <FormControl
              type="email"
              name="email"
              placeholder="email"
              className="form-control my-3"
              onChange={(e) => props.setEmail(e.target.value)}
            />
            <FormControl
              type="password"
              name="password"
              placeholder="password"
              className="my-3"
              onChange={(e) => props.setPassword(e.target.value)}
            />
            <ButtonComp
              variant="danger"
              className="w-100 mt-4"
              label="Login"
              handleClick={props.handleSubmit}
            />
            <Link className="mt-4" to="/">
              Forgot password?
            </Link>
          </form>
        </div>
      </>
    );
  };

  
  export default LoginComp;