import React, { useState } from "react";

// import components

// import styling
import { Modal, Button, FormControl } from "react-bootstrap";

const ModalComp = (props) => {
  const [show, setShow] = useState(false);

  const handleClose = () => setShow(false);
  const handleShow = () => setShow(true);

  return (
    <>
      <Button variant={props.modalButtonOpen} onClick={handleShow}>
        {props.modalButtonOpenLabel}
      </Button>

      <Modal show={show} onHide={handleClose} animation={false}>
        <Modal.Header closeButton>
          <Modal.Title>{props.modalHeading}</Modal.Title>
        </Modal.Header>
        <FormControl type="text" name="text" placeholder="change here" />
        <Modal.Footer>
          <Button variant={props.modalButtonClose} onClick={handleClose}>
            {props.modalButtonCloseLabel}
          </Button>
          <Button variant={props.modalButtonSave} onClick={handleClose}>
            {props.modalButtonSaveLabel}
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};
export default ModalComp;
