import React from "react";

// import component here

// import styling here

// import context here

const BackgroundComp = (props) => {
  const bg = {
    backgroundImage: `${props.background}`,
    backgroundSize: "cover",
    position: "fixed",
    minHeight: "100%",
    minWidth: "100%",
    backgroundPosition: "center",
    boxShadow: "inset 0px 0px 100px 0px #000",
    zIndex: 0,
  };

  return <div style={bg}>{props.children}</div>;
};

export default BackgroundComp;
