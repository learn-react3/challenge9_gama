import React from "react";
import { Link } from "react-router-dom";

// import component here
import ButtonComp from "../Button/Button";

// import styling here
import "./RegisterComp.css";
import { FormControl } from "react-bootstrap";

// import context here

const RegisterComp = (props) => {
  return (
    <>
      <div className="form-reg">
        <h2>REGISTER</h2>
        <form>
          <FormControl
            type="text"
            name="name"
            placeholder="name"
            className="my-3"
            onChange={(e) => props.setName(e.target.value)}
          />
          <FormControl
            type="text"
            name="user name"
            placeholder="user name"
            className="my-3"
            onChange={(e) => props.setUserName(e.target.value)}
          />
          <FormControl
            type="email"
            name="email"
            placeholder="email"
            className="form-control my-3"
            onChange={(e) => props.setEmail(e.target.value)}
          />
          <FormControl
            type="password"
            name="password"
            placeholder="password"
            className="my-3"
            onChange={(e) => props.setPassword(e.target.value)}
          />
          <ButtonComp
            variant="danger"
            className="w-100 mt-4"
            label="Sign Up"
            handleClick={props.handleSubmit}
          />
          <Link className="mt-4" to="/">
            Forgot password?
          </Link>
        </form>
      </div>
    </>
  );
};

export default RegisterComp;
