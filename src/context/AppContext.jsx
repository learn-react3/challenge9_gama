import React, { createContext, useEffect, useState } from 'react';
import jwtDecode from 'jwt-decode';

// Api
import { postLogin } from '../api';

// export context here
export const AuthContext = createContext({});

function useAuthProvider() {
  const token = localStorage.getItem('token');
  const [isAuthenticated, setIsAuthenticated] = useState(() => (token ? true : false));
  const [userId, setUserId] = useState(null);

  const signIn = async (values) => {
    try {
      const result = await postLogin(values);
      const { token } = result;
      const { id } = jwtDecode(token);

      setUserId(id);

      localStorage.setItem('token', token);

      setIsAuthenticated(true);
    } catch (err) {
      console.error(err.message);
    }
  };

  const signOut = () => {
    localStorage.removeItem('token');
    setUserId(null);
    setIsAuthenticated(false);
  };

  return {
    userId,
    isAuthenticated,
    signIn,
    signOut,
  };
}

const AppContext = (props) => {
  const auth = useAuthProvider();

  return <AuthContext.Provider value={auth}>{props.children}</AuthContext.Provider>;
};

export default AppContext;

// Nunggu API Ready
// const isAuth = async () => {
//   try {
//     const response = await fetch("http://localhost:5000/auth/is-verify", {
//       method: "GET",
//       headers: { token: localStorage.token },
//     });

//     const parseRes = await response.json();

//     parseRes === true ? setAuthenticated(true) : setAuthenticated(false);
//   } catch (err) {
//     console.error(err.message);
//   }
// };

// useEffect(() => {
//   isAuth();
// }, []);
