import React, { useContext, useState, useEffect } from 'react';
import './App.css';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';

// import components
import Landing from './pages/Landing/Landing';
import Register from './pages/Register/Register';
import Login from './pages/Login/Login';
import Home from './pages/Home/Home';
import UserProfile from './pages/UserProfile/UserProfile';
import GameDetail from './pages/GameDetail/GameDetail';
import GameList from './pages/GameList/GameList';
import NavbarComp from './components/Navbar/Navbar';

// import context
import { AuthContext } from './context/AppContext';

function App() {
  // const [isAuthenticated, setAuthenticated] = useState(false);

  const { isAuthenticated, signOut } = useContext(AuthContext);

  console.log(isAuthenticated);

  const LoggedOut = (
    <NavbarComp navBg="dark" nav1="home" nav1Label="Home" nav2="" nav2Label="About" nav3="login" nav3Label="Login" nav4="register" nav4Label="Register" />
  );

  const LoggedIn = (
    <NavbarComp
      navBg="primary"
      nav1="home"
      nav1Label="Home"
      nav2="gamedetail"
      nav2Label="Game Detail"
      nav3="gamelist"
      nav3Label="Game List"
      nav4="userprofile"
      nav4Label="Profile"
      withLogoutButton={true}
      onLogOut={signOut}
    />
  );

  // useEffect(() => {
  //   isAuth();
  // }, []);

  return (
    <>
      <Router>
        {isAuthenticated ? LoggedIn : LoggedOut}
        <Switch>
          <Route exact path="/" component={Landing} />

          <Route path="/register" render={(props) => (!isAuthenticated ? <Register /> : <Redirect to="/home" />)} />

          <Route path="/login" render={(props) => (!isAuthenticated ? <Login /> : <Redirect to="/home" />)} />
          <Route path="/home" render={(props) => (isAuthenticated ? <Home /> : <Redirect to="/login" />)} />
          <Route path="/userprofile" render={(props) => (isAuthenticated ? <UserProfile /> : <Redirect to="/login" />)} />
          <Route path="/gamedetail" render={(props) => (isAuthenticated ? <GameDetail /> : <Redirect to="/login" />)} />
          <Route path="/gamelist" render={(props) => (isAuthenticated ? <GameList /> : <Redirect to="/login" />)} />
        </Switch>
      </Router>
    </>
  );
}

export default App;

// const setAuth = (boolean) => {
//   setAuthenticated(boolean);
// };

// const isAuth = () => {
//   try {
//     const parseRes = localStorage.getItem("token");
//     console.log(parseRes);

//     parseRes === true ? setAuthenticated(true) : setAuthenticated(false);
//   } catch (err) {
//     console.error(err.message);
//   }
// };
