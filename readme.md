# FE-app

## How to contribute?

Langkah-langkah yang harus dilakukan untuk memulai berkontribusi mengembangkan proyek ini:

1. Clone repositori

   ```
   git clone https://gitlab.com/binar-fullstack-web-bootcamp/chapter-9/tim-2-pushajadulu/fe-app.git
   ```

   Kemudian masuk ke dalam folder repositorinya

   ```
    cd fe-app
   ```

2. Install dependensi

   ```
    npm install
   ```

3. Jalankan aplikasi dengan perintah berikut

   ```
   npm run dev
   ```

   **Note**: script untuk menjalankan aplikasi ada perubah dari default bawaan create-react-app, detailnya bisa dicek di file `package.json`.

4. Selamat mengembangkan proyek ini. Jangan lupa ikuti git flow yang sudah disepakati ;)
